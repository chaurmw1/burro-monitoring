﻿using System;
using System.Collections.Generic;
using System.Text;

namespace burro_monitoring
{
    class Email
    {
        public string To { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public string Name { get; set; }
        public string From { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
    }
}
