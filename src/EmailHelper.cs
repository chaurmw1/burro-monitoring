﻿using System.Net.Mail;
using System.Text;

namespace burro_monitoring
{
    internal static class EmailHelper
    {
        public static void SendMail(Email email)
        {
            var mail = new MailMessage();
            mail.To.Add(email.To);
            mail.From = new MailAddress(email.From, email.Name, Encoding.UTF8);
            mail.Subject = email.Subject;
            mail.SubjectEncoding = Encoding.UTF8;
            mail.Body = email.Body;
            mail.BodyEncoding = Encoding.UTF8;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;

            var client = new SmtpClient
            {
                Host = email.Host,
            };

            client.Send(mail);
        }
    }
}
