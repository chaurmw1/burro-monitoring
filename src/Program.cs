﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace burro_monitoring
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json", true, true);

            var configuration = builder.Build();

            PollBurroService(configuration);
        }

        private static void PollBurroService(IConfigurationRoot configuration)
        {

            var path = Path.GetFullPath(configuration.GetSection("monitoringfilepath").Value);

            var lastModified = new FileInfo(path).LastWriteTime;

            var interval = DateTime.Now.Subtract(lastModified).TotalMinutes;

            var intervalLimit = Convert.ToInt32(configuration.GetSection("timeinterval").Value);

            if (interval > intervalLimit)
            {
                SendAlert(configuration);
            }
        }

        private static void SendAlert(IConfiguration configuration)
        {
            var email = new Email
            {
                From = configuration.GetSection("fromemail").Value,
                Password = configuration.GetSection("frompassword").Value,
                Host = configuration.GetSection("host").Value,
                Name = "Alert - Burro service is down",
                Body = @"Hi Team,<br/><br/>This is an urgent notification, Burro window service is reported down. Please email to Webops.Team@bbc.com to restart window service <b>Service.File.Transfer</b>
                       at <b>WGB01LB1010.",
                To = configuration.GetSection("toemail").Value,
                Subject = "Urgent Alert: Burro is down"
            };

            EmailHelper.SendMail(email);
        }
    }
}
